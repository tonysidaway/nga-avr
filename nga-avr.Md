# Nga-avr


A 16-bit implementation of Charles Childers' Nga virtual machine, on the Atmel AVR family, intended for use on as wide a range of the family as possible. The code is written in AVR assembler for speed and compactness.


The data cell size is 2 bytes whereas Childers wrote for 4 bytes. All logic and arithmetic are scaled accordingly. Some functions such as multiplication and division are emulated in software on those processors that do not support those functions in hardware.

The design of Nga is such that the size of the instructions need not be exposed, as long as a cell can hold a full instruction address so that instructions like JUMP, CALL and RETURN work. The architecture of the AVR has separate memory spaces for code and RAM, so unlike in Childers' implementation I don't try to pack Nga instructions into a cell. This has implications for the implementation of an assembler, but mostly it simplifies everything.


The 16-bit cell size in this implementation means that byte addresses beyond 64KiB cannot be stored in a data cell, and Childers' FETCH kludge (using negative addresses to make the FETCH opcode perform extra functions) cannot be used because doing so would halve the usable address range to just 32KiB. On the other hand, interface code written in assembler or a high level language can make full use of the AVR's Flash memory internally using the full hardware capabilities of the processor. The instructions IE,IQ and II can be used for this purpose.


For simplicity we use a byte in Flash for each instruction, achieving a reasonable compromise between speed and compactness. Packing as in Childers' implementation is not used here as it's unnecessary. Each Nga instruction is a byte of Flash. Huffman coding might enable better instruction density but this may impact execution speed unacceptably and we could only encode short sequences.


Since there are just 30 instructions in Childers' Nga implementation, there is much space for growth in the VM instruction set so as to tune the VM more fully to the AVR architecture. This variant extends the VM to support fetch and store for byte and even nibble sized data, and provides support for scaling between byte and cell addresses.


This document describes and specifies an implementation of Nga, adapted for the ATtiny85 processor which has 8KiB of Flash for programs, 512B of static RAM for data and 512B of EEPROM. As such, it's probably the smallest machine on which a Nga VM would be useful.


This document is written in Unu format so that the assembler code can be extracted using Childers' Unu tool. It's a minimalist approach to literate programming.


For acknowledgements, copyright and legal information, see Appendix A of this document.


As principal author of this work, author of this new implementation of Nga: copyright Tony Sidaway, 2019.


## AVR registers used

There are 32 8-bit registers, an adjacent pair (with the lower register of the two being even) can be used to store 16-bit values. The top six registers (r26-r31) can be used as three 16-bit index registers (X, Y and Z) in indirect addressing. Z is especially versatile because it alone can be used as an index register in the Program memory (Flash). The registers r26-r31 are commonly addressed in code as XL, XH, YL, YH ZL, and ZH.

Additionally, immediate instructions (ldi, andi, etc) are restricted to r16-r31 (the high registers). r0-r15 (low registers) are still very useful and are always used by this code where appropriate. An immediate operation on a free high register (such as r24,r25, r30 or r31) followed by a mov to a low register is always given preference to tying up a high register permanently.

Finally, r24-r31 are the only registers that are supported by adiw and sbiw, 16-bit immediate add and subtract. This makes r24 and r25 a favourite pair to allocate for roles requiring the highest level of versatility, as they are the only non-index registers that can be used for all instructions.


Z (R31:R30) used both for jump tables and for fetching Nga instructions from flash program memory, and as a general address pointer. This multiple use is unavoidable because Z is the only register pair that can be used with Lpm/Spm and also the only one that can be used with Ijmp/Icall. Z and its two components are also available for general use in the implementation of opcodes and interfaces.


SPH:SPL the Nga address stack pointer, RP and also the AVR hardware stack pointer. This holds a stack of 16-bit byte addresses as in Y. The range of bits actually implemented varies from model to model on the AVR series, and on smaller models SPH is not present. This is a pointer into static RAM. The stack grows downwards in RAM although by convention the stack is referred to as if it's growing upwards.


X (R27:R26) the Nga data stack pointer, SP. This is a pointer into static RAM. Similarly it's referred to as if it's growing upwards even though it grows downwards.


TOSH_R:TOSL_R (TOSLW_R) which are R25:R24 so we can use adiw/sbiw when necessary. This holds the contents of the top of the data stack for the purpose of speed optimization.


Y (R29:R28) the Nga instruction pointer, IP. This holds a 16-bit byte address of the next instruction. After an instruction is fetched it must be incremented.

TEMPH_R:TEMPL_R (TEMPW_R) 8/16-bit scratchpad. ZL and ZH can also be used and there are fewer restrictions, but most of the time a register pair in the lower bank is absolutely fine.

SIGN_R, COUNT_R: registers used by multiplication and division operations.

NGAMASK_R: Bitmask register used to track the current memory type being used as code location. The VM can read and execute NGA code from RAM or EEPROM, enabling new code to be tested on-chip. Using this bitmask expedites speedy processing.

## Memory map

There are three distinct memory maps important to general use of the AVR: Flash, Static RAM and EEPROM. These are brought together in this implementation by the simple mechanism of reserving certain ranges of 256-byte pages in the 16-bit memory map to each of the three types of memory.

Pages 0-EEPROMSTARTH-1: These pages are reserved for the normal data bus of the AVR. The first 32 addresses are the AVR registers. Following that, up to 480 addresses are I/O registers, depending on the model. RAM follows immediately after the final I/O register. By default I set EPROMSTARTH high enough to reserve enough space for the entire static RAM space of the gargantuan ATmega2560 model, 8KIB.

Pages EEPROMSTARTH-FLASHSTARTH-1: these pages are reserved for EEPROM. This type of memory is laborious and rather slow to write but can be read reasonably easily as long as interrupts are disabled during the operation. I use Atmel's recommended safe methods for both read and write to EEPROM. By default I set FLASHSTARTH high enough to reserve the whole EEPROM space of the ATmega2560, 4KiB.

FLASHSTARTH-255: This final range of memory pages is reserved for the onboard Flash. This is read-only memory during normal operation. It is used as program memory by the AVR, and also holds the NGA byte code executed first when the microcontroller is powered on or reset. It can also be used for data pointers to RAM or EEPROM (which is how one would implement variables in nga-avr code) and for large arrays of constant tabular data.

Due to the rationalisation of this memory map, it can be seen that the code of the VM can detect the type of memory being addressed by examining only the upper byte of the 16-bit address.

## Code format

The assembler code here is written in the GNU avr-as syntax supported by avr-gcc and binutils-avr. This affects various trivial matters such as assembler directives, but it should be easy to adapt to the assembler format used by Atmel's own toolchain. I make every effort to reduce the use of GNU-dependent features to a minimum. For practical reasons avr-gcc is used to process the assembler file, rather than avr-as, as this enables use of C include files and other features such as the #define feature, simplifying the management of the build process.
~~~
#include <avr/io.h>

.equ DSTACK, RAMEND
.equ RSTACK, RAMEND - 0x40
.equ EEPROMSTARTH, 34
.equ FLASHSTARTH, 38

#include "nga_avr_registers.h"

.equ RAMBIT,     0
.equ FLASHBIT,   1
.equ EEPROMBIT,  2
.equ RAMMASK,    1 << RAMBIT
.equ FLASHMASK,  1 << FLASHBIT
.equ EEPROMMASK, 1 << EEPROMBIT

~~~
## Reset and interrupt vectors
16 words at the start of Flash.
This is okay for ATtiny85, but larger models
have 4 bytes per interrupt vector to accommodate the larger address field of the jmp instruction. FIXME.
~~~
.org 0
Reset:
	rjmp _Start
	reti
	reti
	reti
	reti
	reti
	reti
	reti
	reti
	reti
	reti
	reti
	reti
	reti
	reti
	reti
~~~
## Start
~~~
_Start:
	cli
	ldi ZL,lo8(RSTACK)
	out _SFR_IO_ADDR(SPL),ZL
	ldi ZL,hi8(RSTACK)
	out _SFR_IO_ADDR(SPH),ZL
	ldi YL,lo8(PROGSTART)
	ldi YH,hi8(PROGSTART)
	ldi ZL,FLASHMASK
	mov NGAMASK_R,ZL
	ldi XL, lo8(DSTACK)
	ldi XH, hi8(DSTACK)
	rjmp NextInstByte
~~~
Dispatch table. 2 words per entry. 2 clocks each.
~~~
OPTAB:
	rjmp _instNop		; 0
	rjmp _instLit
	rjmp _instDup
	rjmp _instDrop
	rjmp _instSwap
	rjmp _instPush		; 5
	rjmp _instPop
	rjmp _instJump
	rjmp _instCall
	rjmp _instCcall
	rjmp _instReturn	; 10 0x0a
	rjmp _instEq
	rjmp _instNeq
	rjmp _instLt
	rjmp _instGt
	rjmp _instFetch		; 15 0x0f
	rjmp _instStore
	rjmp _instAdd
	rjmp _instSub
	rjmp _instMul
	rjmp _instDivmod	; 20 0x14
	rjmp _instAnd
	rjmp _instOr
	rjmp _instXor
	rjmp _instShift
	rjmp _instZret		; 25 0x19
	rjmp _instEnd
	rjmp _instIE
	rjmp _instIQ
	rjmp _instII		; 29 0x1d
	rjmp _instCells		; 30 0x1e
	rjmp _instFetch8
	rjmp _instStore8
	rjmp _instFetch4
	rjmp _instStore4	; 34 0x22
	rjmp _instChoose	; 35 0x23
~~~
## Dispatch loop


The heart of any VM is the dispatch loop, which fetches instructions from program memory and sends them to be interpreted.

Because the AVR instruction set lacks add-immediate instructions, the practice is to negate the constant and subtract it when computing a vector into a jump table.


We're addressing program (Flash) memory and Ijmp expects a word address, hence the use of pm_lo8() and pm_hi8() macros here. The use of these macros is an unavoidable Gnuism, which must be adapted if you want to use a different assembler which doesn't provide those same macros or which understands the AVR architecture better.

NextInstByte sequence
~~~
SetupInstByte:
	ldi ZL,RAMMASK
	mov NGAMASK_R,ZL
	Movw Z,Y
	cpi ZH,FLASHSTARTH
	brsh SetNextFromFlash
	cpi ZH,EEPROMSTARTH
	brsh SetNextFromEeprom
NextInstByte:
	movw Z,Y
	adiw Y,1
	sbrc NGAMASK_R,RAMBIT
	ld TEMPL_R,Z
	sbrc NGAMASK_R,FLASHBIT
	Lpm TEMPL_R,Z
	sbrc NGAMASK_R,EEPROMBIT
	rcall FetchEepromByte
InterpretIt:
	Mov ZL,TEMPL_R ; Nga instruction.
	Clr ZH
	Subi ZL,pm_lo8(-(OPTAB)) ; Add word address
	Sbci ZH,pm_hi8(-(OPTAB)) ; of jump table.
	Ijmp ; Expects a 16-bit word address         2clks
SetNextFromFlash:
	ldi ZL,FLASHMASK
	mov NGAMASK_R,ZL
	Subi YH,FLASHSTARTH
	rjmp NextInstByte
SetNextFromEeprom:
	ldi ZL,EEPROMMASK
	mov NGAMASK_R,ZL
	Subi YH,EEPROMSTARTH
	rjmp NextInstByte
NextFromEeprom:
	movw Z,Y
FetchEepromByte:
	in TEMPH_R,_SFR_IO_ADDR(SREG)
	Cli
WaitERead:
	Sbic _SFR_IO_ADDR(EECR),EEPE
	Rjmp WaitERead
	out _SFR_IO_ADDR(EEARH),ZH
	out _SFR_IO_ADDR(EEARL),ZL
	sbi _SFR_IO_ADDR(EECR),EERE
	in TEMPH_R,_SFR_IO_ADDR(EEDR)
	out _SFR_IO_ADDR(SREG),TEMPL_R
	ret
~~~
## Instruction code


As far as is practicable, the instruction code is presented in the same order (opcode order) as in the C source of Childers' own Nga implementation. Where this isn't the case, this is due to my exploiting assembler tricks such as drop-throughs to share code between two or more instructions (such as ccall, call and jump). I'll describe the instructions in the text first, in Childers' order and numbering whenever possible, followed by a code block which may implement several instructions in a completely different order. Where opcodes are implemented out of order this misordering will be clearly noted to improve readability.


The same opcode numbers as Childers' Nga are used for the convenience of those comparing the different implementations. This also simplifies disassembly.


### NOP (): Opcode 0.
Do nothing. In practice it makes sense to simply jump directly to NextInstByte. Nevertheless this alternative implementation is included for completeness.


### LIT (-n): Opcode 1.
Consume two bytes from the instruction stream and push them onto the stack.


In this implementation, LIT drops through into NOP for compactness.
~~~
_instLit:
	St -X,TOSH_R
	St -X,TOSL_R
	Movw Z,Y
	sbrc NGAMASK_R, FLASHBIT
	rjmp LitFromFlash
	sbrc NGAMASK_R,EEPROMBIT
	rjmp LitFromEeprom
	Ld TOSL_R,Z+
	Ld TOSH_R,Z+
	Movw Y,Z
	rjmp NextInstByte
LitFromEeprom:
	rcall FetchEepromByte
	mov TOSL_R,TEMPL_R
	adiw Z,1
	rcall FetchEepromByte
	mov TOSH_R,TEMPL_R
	adiw Z,1
	movw Y,Z
	rjmp NextInstByte
LitFromFlash:
	Lpm TOSL_R,Z+
	Lpm TOSH_R,Z+
	movw Y,Z
; *****DROP THROUGH!*****
_instNop:
	rjmp NextInstByte
~~~
### DUP (n-nn): Opcode 2.
Duplicate the item at the top of the data stack.
~~~
_instDup:
	St -X,TOSH_R
	St -X,TOSL_R
	rjmp NextInstByte
~~~
### DROP (n-): Opcode 3.
Remove one item from the top of the data stack.
~~~
_instDrop:
	Ld TOSL_R,X+
	Ld TOSH_R,X+
	rjmp NextInstByte
~~~
### SWAP (mn-nm): Opcode 4.
Swap the top two items on the data stack.
~~~
_instSwap:
	Movw TEMPW_R,TOSW_R
	Ld TOSL_R,X+
	Ld TOSH_R,X+
	St -X,TEMPH_R
	St -X,TEMPL_R
	rjmp NextInstByte
~~~
### PUSH (n-, a:-n): Opcode 5.
Remove one item from the top of the data stack and push it onto the top of the return stack.
~~~
_instPush:
	push TOSH_R
	push TOSL_R
	Ld TOSL_R,X+
	Ld TOSH_R,X+
	rjmp NextInstByte
~~~
### POP (-n, a:n-): Opcode 6.
Remove one item from the top of the return stack and push it onto the top of the data stack.
~~~
_instPop:
	St -X,TOSH_R
	St -X,TOSL_R
	pop TOSL_R
	pop TOSH_R
	rjmp NextInstByte
~~~
### JUMP (n-):  Opcode 7.
Take an item from the top of the data stack and move it to the IP.
### CALL (n- a:-n): Opcode 8.
Save the IP on the return stack, then take an item from the top of the data stack and move it to the IP.
### CCALL (nf- a:-n|nf-): Opcode 9.
Take the flag from the top of the data stack. If it's non-zero, proceed as CALL. If it's zero, remove the call address from the data stack and no nothing.


In this implementation, Ccall, Call and Jump share code.
~~~
_instCcall:
	Movw TEMPW_R,TOSW_R
	Ld TOSL_R,X+ ; Remove flag from NOS
	Ld TOSH_R,X+ ; for testing
	Adiw TOSW_R,0
	Breq _Cleanup ; No call if flag zero
	Movw TOSW_R,TEMPW_R
; *****DROP THROUGH!*****
_instCall:
	sbrc NGAMASK_R,FLASHBIT
	sbci YH,-FLASHSTARTH
	sbrc NGAMASK_R,EEPROMBIT
	sbci YH,-EEPROMSTARTH
	push YH ; Store return address
	push YL ; on return stack
; *****DROP THROUGH!*****
_instJump:
	Movw Y,TOSW_R
	rjmp SetupInstByte
_Cleanup:
	Ld TOSL_R,X+ ; Drop
	Ld TOSH_R,X+
	rjmp NextInstByte
~~~
### RETURN (n-):  Opcode 10.
Take an item from the top of the return stack and move it to the IP.
### ZRET (- | f- a:n-): Opcode 25. Presented out of opcode order.
If the item on the top of the data stack is zero, take an item of the top of the return stack and move it to the IP. If the item on the top of the data stack is non-zero, do nothing.


In this implementation, Zret drops through into Return for compactness. So Zret is defined out of opcode order.
~~~
_instZret:
	Adiw TOSW_R,0
	Brne _ZretNo ; No action
	Ld TOSL_R,X+ ; Drop
	Ld TOSH_R,X+
; *****DROP THROUGH!*****
_instReturn:
	pop YL
	pop YH
	rjmp SetupInstByte
_ZretNo:
	rjmp NextInstByte
~~~
### EQ (mn-f): . Opcode 11.
Compare the two items on the top of the data stack and replace them with a flag: -1 if they're equal, 0 if they're not equal.


### NEQ (mn-f): Opcode 12.
Compare the two items on the top of the data stack and replace them with a flag: -1 if they're not equal, 0 if they're equal.


### LT (nn--f): Opcode 13.
Compare (16 bits, signed) the top two items on the data stack. If the difference n1-n2 is negative push -1, otherwise push 0.


### GT (nn--f): Opcode 14.
Compare (16 bits, signed) the top two items on the data stack. If the difference n2-n1 is negative push -1, otherwise push 0.


In this implementation, Eq, Neq, Lt and Gt share code and there are a couple of drop-thoughs.
~~~
_instEq:
	Ld TEMPL_R,X+
	Ld TEMPH_R,X+
	Sub TOSL_R,TEMPL_R
	Sbc TOSH_R,TEMPH_R
	Breq _True
	Rjmp _False


_instNeq:
	Ld TEMPL_R,X+
	Ld TEMPH_R,X+
	Sub TOSL_R,TEMPL_R
	Sbc TOSH_R,TEMPH_R
	Breq _False
	Rjmp _True


_instLt:
	Ld TEMPL_R,X+
	Ld TEMPH_R,X+
	Sub TOSL_R,TEMPL_R
	Sbc TOSH_R,TEMPH_R
	Brlt _True
; *****DROP THROUGH!*****
_False:
	Clr TOSL_R
	Clr TOSH_R
	rjmp NextInstByte

_instGt:
	Ld TEMPL_R,X+
	Ld TEMPH_R,X+
	Sub TOSL_R,TEMPL_R
	Sbc TOSH_R,TEMPH_R
	Brpl _False
; *****DROP THROUGH!*****
_True:
	Ser TOSL_R
	Ser TOSH_R
	rjmp NextInstByte


~~~
### FETCH (n-n): Opcode 15.
Pop a cell address from the data stack and push the contents of the cell to the data stack.


NOTE: This implementation does not support the kludge whereby negative addresses perform extra functions, as this would unacceptably impact the primary use of FETCH.
~~~
_instFetch:
	Movw Z,TOSW_R
	cpi ZH,FLASHSTARTH
	brsh _instFetchFlash
	cpi ZH,EEPROMSTARTH
	brsh _instFetchEeprom
	Ld TOSL_R,Z+
	Ld TOSH_R,Z+
	rjmp NextInstByte

_instFetchFlash:
	subi ZH,FLASHSTARTH
	lpm TOSL_R,Z+
	lpm TOSH_R,Z+
	rjmp NextInstByte

_instFetchEeprom:
	subi ZH,EEPROMSTARTH
	rcall FetchEepromByte
	mov TOSL_R,TEMPL_R
	adiw Z,1
	rcall FetchEepromByte
	mov TOSH_R,TEMPL_R
	rjmp NextInstByte
	
~~~
### STORE (mn-): Opcode 16.
Pop a cell address from the data stack and pop the next cell from the data stack and store its value at the cell address indicated.
~~~
_instStore:
	Movw Z,TOSW_R
	cpi ZH,FLASHSTARTH
	brsh _instStoreFlash
	cpi ZH,EEPROMSTARTH
	brsh _instStoreEeprom
	Ld TOSL_R,X+
	Ld TOSH_R,X+
	Std Z+1,TOSH_R
	St Z,TOSL_R
	Ld TOSL_R,X+ ; Drop
	Ld TOSH_R,X+
	rjmp NextInstByte
	
; We just fake Flash and Eeprom writes for now
_instStoreFlash:
_instStoreEeprom:
	Ld TOSL_R,X+
	Ld TOSH_R,X+
	Ld TOSL_R,X+ ; Drop
	Ld TOSH_R,X+
	rjmp NextInstByte
	
~~~
### ADD (nn-n): Opcode 17.
Compute the signed 16-bit sum of the top two items on the data stack.
~~~
_instAdd:
	Ld TEMPL_R,X+
	Add TOSL_R,TEMPL_R
	Ld TEMPH_R,X+
	Adc TOSH_R,TEMPH_R
	rjmp NextInstByte
	
~~~
### SUB (nn-n): Opcode 18.
Compute the signed 16-bit difference of the top two items on the data stack.
~~~
_instSub:
	Ld TEMPL_R,X+
	Sub TOSL_R,TEMPL_R
	Ld TEMPH_R,X+
	Sbc TOSH_R,TEMPH_R
	rjmp NextInstByte
	
~~~
### MUL(nn-n): Opcode 19.
16-bit signed multiply. Take the top two items from the data stack, multiply them and place the result on the data stack. The method used is described in Atmel's Application Note AVR200: Multiply and Divide Routines.
~~~
_instMul:
	Movw Z,TOSW_R
	Ld TOSL_R,X+
	Ld TOSH_R,X+
	Clr TEMPH_R
	Sub TEMPL_R,TEMPL_R
	Ldi COUNT_R,16
_Mul1:
	Brcc _Mul2
	Add TEMPL_R,ZL
	Adc TEMPH_R,ZH
_Mul2: 
	Sbrc TOSL_R,0
	Sub TEMPL_R,YL
	Sbrc TOSL_R,0
	Sbc TEMPH_R,YH
	Asr TEMPH_R
	Ror TEMPL_R
	Ror TOSH_R
	Ror TOSL_R
	Dec COUNT_R
	Brne _Mul1
	rjmp NextInstByte
	
~~~
### DIVMOD(nn-nn): Opcode 20.
16-bit signed division with remainder. Take the top two items from the data stack, multiply them and place the result on the data stack. The method used is described in Atmel's Application Note AVR200: Multiply and Divide Routines.
~~~
_instDivmod:
	movw	Z,TOSW_R	;divisor to Z
	ld	TOSL_R,X+		;dividend to TOSW_R
	Ld	TOSH_R,X+
div16s:
	mov	SIGN_R,TOSH_R	;move dividend High to sign register
	eor	SIGN_R,ZH		;xor divisor High with sign register
	sbrs	TOSH_R,7		;if MSB in dividend set
	rjmp	d16s_1
	com	TOSH_R		;    change sign of dividend
	com	TOSL_R		
	subi	TOSL_R,lo8(-1)
	sbci	TOSH_R,hi8(-1)
d16s_1:
	sbrs	ZH,7		;if MSB in divisor set
	rjmp	d16s_2
	com	ZH		;    change sign of divisor
	com	ZL		
	subi	ZL,lo8(-1)
	sbci	ZH,hi8(-1)
d16s_2:
	clr	TEMPL_R		;clear remainder Low byte
	sub	TEMPH_R,TEMPH_R;clear remainder High byte and carry
	ldi	COUNT_R,17	;init loop counter

d16s_3:
	rol	TOSL_R		;shift left dividend
	rol	TOSH_R
	dec	COUNT_R		;decrement counter
	brne	d16s_5		;if done
	sbrs	SIGN_R,7		;    if MSB in sign register set
	rjmp	d16s_4
	com	TOSH_R		;        change sign of result
	com	TOSL_R
	subi	TOSL_R,lo8(-1)
	sbci	TOSH_R,hi8(-1)
d16s_4:
	rjmp	Finished	;    return
d16s_5:
	rol	TEMPL_R		;shift dividend into remainder
	rol	TEMPH_R
	sub	TEMPL_R,ZL	;remainder = remainder - divisor
	sbc	TEMPH_R,ZH	;
	brcc	d16s_6		;if result negative
	add	TEMPL_R,ZL	;    restore remainder
	adc	TEMPH_R,ZH
	clc			;    clear carry to be shifted into result
	rjmp	d16s_3		;else
d16s_6:
	sec			;    set carry to be shifted into result
	rjmp	d16s_3
Finished:
	st	-X,TEMPH_R
	st	-X,TEMPL_R
	rjmp NextInstByte
	
~~~
### AND (nm-o): Opcode 21.
Compute the bitwise AND of the two top items on the data stack.
~~~
_instAnd:
	Ld TEMPL_R,X+
	And TOSL_R,TEMPL_R
	Ld TEMPH_R,X+
	And TOSH_R,TEMPH_R
	rjmp NextInstByte
	
~~~
### OR (nm-o): Opcode 22.
Compute the bitwise OR of the two top items on the data stack.
~~~
_instOr:
	Ld TEMPL_R,X+
	Or TOSL_R,TEMPL_R
	Ld TEMPH_R,X+
	Or TOSH_R,TEMPH_R
	rjmp NextInstByte
	
~~~
### XOR (nm-o): Opcode 23.
Compute the bitwise EXCLUSIVE OR of the two top items on the data stack.
~~~
_instXor:
	Ld TEMPL_R,X+
	Eor TOSL_R,TEMPL_R
	Ld TEMPH_R,X+
	Eor TOSH_R,TEMPH_R
	rjmp NextInstByte
	
~~~
### SHIFT (nm-o): Opcode 24..
If m is positive, shift n right by m bits.
If m is negative, shift n left by -m bits.
~~~
_instShift:
	Ld TEMPL_R,X+
	Ld TEMPH_R,X+
	Adiw TOSW_R,0
	Breq _ShiftDone
	Brpl _ShiftRight
_ShiftLeft:
	Add TEMPL_R,TEMPL_R
	Adc TEMPH_R,TEMPH_R
	Inc TOSL_R
	Brne _ShiftLeft
_ShiftRight:
	Asr TEMPH_R
	Ror TEMPL_R
	Dec TOSL_R
	Brne _ShiftRight
_ShiftDone:
	rjmp NextInstByte
	
~~~
### ZRET Opcode 25. This instruction is described and implemented earlier in the text.


### END (-): Nothing more to do. Opcode 26.
~~~
_instEnd:
; Whoever set this running probably had an idea on what
; the processor should do next.
	Ret
~~~
### IE (-n): Opcode 27.
Put the number of I/O devices available onto the data stack.
~~~
_instIE:
	St -X,TOSH_R
	St -X,TOSL_R
	Clr TOSL_R ; There are no I/O devices.
	Clr TOSH_R
	rjmp NextInstByte
	
~~~
### IQ (n-o o): Opcode 28.
Query an I/O device.
~~~
_instIQ:
	Ser TOSL_R ; There are no valid queries.
	Ser TOSH_R
	rjmp NextInstByte
	
~~~
### II (n-): Opcode 29.
Invoke I/O device.
~~~
_instII:
	Ser TOSL_R ; There are no valid devices.
	Ser TOSH_R
	rjmp NextInstByte
	
~~~
### CELLS (-): Opcode 30.
Turn TOS from a cell offset to a byte offset.
It's a hard-coded *2 multiplication here.
~~~
_instCells:
	lsl TOSL_R
	rol TOSH_R
	rjmp NextInstByte
~~~
### FETCH8 (n-n): Opcode 31.
Pop a byte address from the data stack and push the contents of the byte (as a cell) to the data stack.

~~~
_instFetch8:
	Movw Z,TOSW_R
	cpi ZH,FLASHSTARTH
	brsh _instFetch8Flash
	cpi ZH,EEPROMSTARTH
	brsh _instFetch8Eeprom
	Ld TOSL_R,Z
	Clr TOSH_R
	rjmp NextInstByte

_instFetch8Flash:
	subi ZH,FLASHSTARTH
	lpm TOSL_R,Z
	clr TOSH_R
	rjmp NextInstByte

_instFetch8Eeprom:
	subi ZH,EEPROMSTARTH
	rcall FetchEepromByte
	mov TOSL_R,TEMPL_R
	Clr TOSH_R
	rjmp NextInstByte
	
~~~
### STORE8 (mn-): Opcode 32.
Pop a byte address from the data stack and pop the next cell from the data stack and store the value in its lower byte at the byte address indicated.
~~~
_instStore8:
	Movw Z,TOSW_R
	cpi ZH,FLASHSTARTH
	brsh _instStore8Fl
	cpi ZH,EEPROMSTARTH
	brsh _instStore8Ee
	Ld TOSL_R,X+
	Ld TOSH_R,X+
	St Z,TOSL_R
	Ld TOSL_R,X+ ; Drop
	Ld TOSH_R,X+
	rjmp NextInstByte
	
; We just fake Flash and Eeprom writes for now
_instStore8Fl:
_instStore8Ee:
	Ld TOSL_R,X+
	Ld TOSH_R,X+
	Ld TOSL_R,X+ ; Drop
	Ld TOSH_R,X+
	rjmp NextInstByte
	
~~~
### FETCH4 (n-n): Opcode 33.
Pop a nibble address from the data stack and push the contents of the nibble (as a cell) to the data stack.

~~~
_instFetch4:
	Movw Z,TOSW_R
	bst ZL,0
	lsr ZH
	ror ZL
	cpi ZH,FLASHSTARTH
	brsh _instFetch4Flash
	cpi ZH,EEPROMSTARTH
	brsh _instFetch4Eeprom
	Ld TOSL_R,Z
_instFetch4Cleanup:
	clr TOSH_R
	in TEMPL_R,_SFR_IO_ADDR(SREG)
	sbrc TEMPL_R,SREG_T
	swap TOSL_R
	andi TOSL_R,0x0f
	rjmp NextInstByte

_instFetch4Flash:
	subi ZH,FLASHSTARTH
	lpm TOSL_R,Z
	rjmp _instFetch4Cleanup

_instFetch4Eeprom:
	subi ZH,EEPROMSTARTH
	rcall FetchEepromByte
	mov TOSL_R,TEMPL_R
	rjmp _instFetch4Cleanup

~~~
### STORE4 (mn-): Opcode 34.
Pop a nibble address from the data stack and pop the next cell from the data stack and store the value in its lowest nibble at the nibble address indicated.
~~~
_instStore4:
	Movw Z,TOSW_R
	bst ZL,0
	lsr ZH
	ror ZL
	cpi ZH,FLASHSTARTH
	brsh _instStore4Fl
	cpi ZH,EEPROMSTARTH
	brsh _instStore4Ee
	Ld TOSL_R,X+
	Ld TOSH_R,X+
	ld TOSH_R,Z
	rcall AdjustNibbles
	St Z,TOSL_R
	Ld TOSL_R,X+ ; Drop
	Ld TOSH_R,X+
	rjmp NextInstByte
	
; We just fake Flash and Eeprom writes for now
_instStore4Fl:
_instStore4Ee:
	Ld TOSL_R,X+
	Ld TOSH_R,X+
	Ld TOSL_R,X+ ; Drop
	Ld TOSH_R,X+
	rjmp NextInstByte

AdjustNibbles:
	brtc _instStore4EvenNibble1
	swap TOSH_R
	swap TOSL_R
_instStore4EvenNibble1:
	andi TOSH_R,0xf0
	andi TOSL_R,0x0f
	or TOSL_R,TOSH_R
	brtc _instStore4EvenNibble2
	swap TOSL_R
_instStore4EvenNibble2:
	ret

~~~
### CHOOSE (nqq-): Opcode 35.
If the flag is True (-1), call the first quote, otherwise call the second.
~~~
_instChoose:
	ld TEMPL_R,X+
	ld TEMPH_R,X+
	ld ZL,X+
	ld ZH,X+
	adiw Z,0
	brne _instChooseTrue
	rjmp _instCall
_instChooseTrue:
	movw TOSW_R,TEMPW_R
	rjmp _instCall
~~~
## Appendix A


### Copyright
~~~

#* Copyright 2019 Tony Sidaway: implementation in AVR assembler only.


#* From Charles Childers' implementation of Nga in C:


#* Nga derives from my earlier work on Ngaro. The following block lists the people who helped work on the C implementation.
#* Nga
#* Copyright (c) 2008 - 2019, Charles Childers 
#* Copyright (c) 2009 - 2010, Luke Parrish
#* Copyright (c) 2010,        Marc Simpson
#* Copyright (c) 2010,        Jay Skeer
#* Copyright (c) 2011,        Kenneth Keating

~~~
### ISC licensed
~~~
#* Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the copyright notice and this permission notice appear in all copies
#* THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
~~~
~~~
.org 0x0800
PROGSTART:
~~~
