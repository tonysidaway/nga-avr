; Register names for nga_avr

#define SIGN_R r0

#define TEMPL_R r2
#define TEMPH_R r3
#define TEMPW_R TEMPL_R
#define NGAMASK_R r5

#define COUNT_R r16

#define TOSL_R r24
#define TOSH_R r25
#define TOSW_R TOSL_R
