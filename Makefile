elf: nga-avr.elf

nga-avr.elf: nga-avr.s
	avr-gcc -g -nostartfiles -mmcu=attiny85   nga-avr.s -o nga-avr.elf
nga-avr.s: nga-avr.S
	avr-gcc -S -mmcu=attiny85   nga-avr.S > nga-avr.s
nga-avr.S: nga-avr.Md
	retro-unu nga-avr.Md >nga-avr.S
sim: elf
	simavr -m attiny85 -g nga-avr.elf 
db: elf
	avr-gdb --symbols=nga-avr.elf
clean:
	-rm -f nga-avr.S nga-avr.s nga-avr.elf
objdump: elf
	avr-objdump -S nga-avr.elf



